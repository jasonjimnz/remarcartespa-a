# README #

Este documento readme, explica un poco la estructuración seguida para el desarrollo del proyecto

### ¿Para qué sirve este repositorio? ###

RemarcArtEspaña es una plataforma de impulso al turismo mediante artistas (nacionales e internacionales).
Beta 1.0
(https://bitbucket.org/jasonjimnz/remarcartespa-a)

### ¿Cómo lo pongo en marcha? ###

Necestarás Apache o Ngix para PHP (recomendable PHP 5.4 o mayor, sino el API dará error ya que utiliza una función que no está en versiones menores a la 5.4 de PHP) y motor de base de datos MySQL.

Para la plataforma móvil hay que importar la carpeta del proyecto e importar los dos proyectos y después en Eclipse o IntelliJ Idea cambiar las referencias y que no de error al compilar.

La plataforma web, está desarrollada en PHP sin frameworks, el modelo de base de datos está construido en Django y actualmente utiliza motor de BBDD MySQL, incluye una conexión a una base de datos de prueba, de la cual se puede obtener el modelo de datos

La plataforma móvil utiliza Apache Cordova, incluye login y registro de usuarios, y maquetación del estado final que tendrá la aplicación.


### Información de contacto técnico ###

- info@jasonjimnzdev.es
- jasonjimenezcruz@gmail.com