

var controller = {
	init: function(){

		$.each($('div[view]'), function(){
			if($(this).attr('view') != 'side'){

				screens.push($(this).attr('view'));
				if($(this).hasClass('oculto') == false){
					visibleScreen = $(this).attr('view')
				}
			}
		});
		console.log(screens)
		console.log("Controller OK");
	},

	cambio_pantalla: function(destino){
		var origen = ''
		$.each(screens, function(){
			if($('div[view='+this+']').hasClass('oculto') == false){
				origen = this;
				return this;
			}
		})

		if(origen == destino){
			return false;
		}

		$('div[view='+origen+']').fadeOut('slow', function(){
			lastScreen=origen
			$('div[view='+origen+']').addClass('oculto');
			$('div[view='+destino+']').removeClass('oculto');
		});

		setTimeout(function(){
			$('div[view='+origen+']').removeAttr('style');
		}, 1000);
	}

}

	$(document).on('click', '.r_iniciarSesion', function(){
		//controller.cambio_pantalla('home');
		model.login($('#login_email').val() , $('#login_pass').val())
	});

	$(document).on('click', '.r_registrarse', function(){
		controller.cambio_pantalla('registro');
	});

	$(document).on('click', '.r_registro', function(){
		var usuario = $('#registro_email');
		var pass = $('#registro_auth');
		var value = $('#registro_value');

		if(usuario.val() == '' || pass.val() == '' || value.val() == ''){
			navigator.notification.alert("Todos los campos son necesarios");
		}

		if(pass.val() != value.val()){
			navigator.notification.alert("Todos los campos son necesarios");
		}
		validatePass(document.getElementById('registro_auth'));
		if(errorForms==false){
			model.register(usuario.val(), pass.val());
		}
	});

	$(document).on('click', '.r_atras', function(){
		controller.cambio_pantalla('login');
		if(menu == true){
			sacarMenu();
		}
	});

	$(document).on('click', '.r_perfil', function(){
		controller.cambio_pantalla('perfil');
		if(menu == true){
			sacarMenu();
		}
	});

	$(document).on('click' ,'.r_home', function(){
		controller.cambio_pantalla('home');
		if(menu == true){
			sacarMenu();
		}
	});

	$(document).on('click', '.r_cerrarSesion', function(){
		controller.cambio_pantalla('login');
		if(menu == true){
			sacarMenu();
		}
	});

	$(document).on('click', '.r_ilustraciones', function(){
		controller.cambio_pantalla('home');
		if(menu == true){
			sacarMenu();
		}
	});

	$(document).on('click', '.r_acerca', function(){
		controller.cambio_pantalla('acerca');
		if(menu == true){
			sacarMenu();
		}
	});


	function validatePass(campo) {
	    var RegExPattern = /(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,10})$/;
	    var errorMessage = 'La contraseña tiene que contener caracteres alfanuméricos.';
	    if ((campo.value.match(RegExPattern)) && (campo.value!='')) {
	        errorForms= false;
	    } else {
	        alert(errorMessage);
	        errorForms= true;
	    } 
	}
