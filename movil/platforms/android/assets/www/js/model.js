//var Callurl = 'localhost:88/reinventar/api.php?orden=';
//var Callurl = 'http://172.17.5.76:88/reinventa/api.php?orden=';
//Hostinger
var Callurl = 'http://reinventarteespana.esy.es/api.php?orden=';

var model = {
	init: function(){
		console.log("Iniciado");
	},

	login:function(user,pass){
		var call = $.ajax({
			url: Callurl+'login',
			method: "POST",
			data: '{"login":"'+user+'", "auth":"'+pass+'"}',
			dataType:"json",
			contentType:'application/json',
			crossDomain: true,
			async:false,
			success:function(response){
				userdata=response;
				controller.cambio_pantalla('home');
				//window.alert(response.nombre);
			},
			error:function(jqXHR, err){
				console.log(err);
			},
			statusCode:{
				200: function(){
					console.log(200)
				},

				201: function(){
					console.log(201)
				},

				400: function(){
					console.log(400)
				},

				404: function(){
					console.log(404)
					navigator.notification.alert('Login incorrecto');
				},

				500: function(){
					console.log(500)
				}
			}
		});
	},

	register:function(user,pass){
		$.ajax({
			url: Callurl+'registro',
			method: "POST",
			data: '{"login":"'+user+'", "auth":"'+pass+'"}',
			dataType:"json",
			contentType:'application/json',
			crossDomain: true,
			async:false,
			success:function(){
				
				
				//window.alert(response.nombre);
			},
			error:function(jqXHR, err){
				console.log(err);
			},
			statusCode:{
				200: function(){
					console.log(200)
				},

				201: function(){
					console.log(201);
					navigator.notification.alert('Has creado correctamente tu cuenta, ahora puedes acceder a la plataforma');
					controller.cambio_pantalla('login');
				},

				400: function(){
					console.log(400)
				},

				404: function(){
					console.log(404)
					navigator.notification.alert('Login incorrecto');
				},

				500: function(){
					console.log(500)
				}
			}
		});
	}
}