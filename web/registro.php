 <!DOCTYPE html>
<?php 

	include 'config/config.php';
	include 'includes/head.php';
	if(isset($_POST['order'])){
		$respuesta = $usuario->nuevoUsuario($db, $_POST['login'], $_POST['pass']);

		if($respuesta->rowCount() != 0){
			header("Location: index.php");
		}
	}

 ?>

<body style="padding: 10% 0;">
 		<div class="row">
 			<div class="col-lg-6 col-lg-offset-3">
 				<div class="panel panel-default">
 					<div class="panel-heading">
 						<h3 class="panel-title text-center">Registro</h3>
 					</div>
 					<div class="panel-body">
 						<form action="registro.php" method="POST">
 							<input type="hidden" name="order" value="true">
 							<div class="form-group">
 								<label>Login:</label>
 								<input type="text" placeholder="Login" name="login" class="form-control">
 							</div>

 							<div class="form-group">
 								<label>Contraseña:</label>
 								<input type="text" placeholder="Contraseña" name="pass" class="form-control">
 							</div>

 							<div class="form-group">
 								<label>Confirmar contraseña:</label>
 								<input type="text" placeholder="Confirmar contraseña" name="value" class="form-control">
 							</div>

 							<a href="#" class="btn btn-block btn-primary registro">
 								<i class="glyphicon glyphicon-log-in"></i> Crear cuenta
 							</a>
 						</form>
 					</div>
 				</div>
 			</div>
 		</div>
 	</body>

 	<script type="text/javascript">
 	$(document).ready(function(){
 		$(document).on('click', '.registro', function(){
 			var errores = false;

 			var login = $('input[name=login]');
 			var pass = $('input[name=pass]');
 			var val = $('input[name=value]');

 			$.each($('input'), function(){
 				if($(this).val() == ''){
 					errores = true;
 					$(this).css("border","1px solid red");
 				} else {
 					$(this).removeAttr("style");
 				}
 			});

 			if(pass.val() != val.val()){
 				pass.css("border","1px solid red");
 				val.css("border","1px solid red");
 			} else {
 				pass.removeAttr('style');
 				val.removeAttr('style');
 			}

 			if(errores == false){
 				$('form').submit();
 			}
 		});
 	});
 	</script>
