<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reinventarte</title>

	<!-- Estilos Bootstrap -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

	<!--- Scripts JS -->
	<script type="text/javascript" src="libs/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="js/npm.js"></script>-->
	<script type="text/javascript" src="js/scripts.js"></script>


	<style type="text/css">
		.botonEnlaces{text-decoration: none !important; color: inherit !important;}
	</style>

</head>