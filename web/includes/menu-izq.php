<!-- Cuerpo -->
	<section class="row">
		<div class="col-lg-11">
			<div class="col-lg-3">
				<div class="panel panel-default text-center">
					<div class="panel-heading">
						<span class="panel-title">ReinventartEspaña</span>
					</div>
					<div class="panel-body">
						<div class="panel panel-default">
							<div class="panel-heading">
								<span class="panel-title">Secciones</span>
							</div>

							<div class="panel-body">
								<ul class="list-group">
									<li class="btn btn-default btn-block list-group-item pull-left "><a class="botonEnlaces" href="#"><i class="glyphicon glyphicon-home"></i> Inicio <span class="sr-only">(actual)</span></a></li>
							        <li class="btn btn-default btn-block list-group-item pull-left loadUsuarios"><a class="botonEnlaces" href="#"><i class="glyphicon glyphicon-th-list"></i> Listado Usuarios</a></li>
							        <li class="btn btn-default btn-block list-group-item pull-left loadPerfiles"><a class="botonEnlaces" href="#"><i class="glyphicon glyphicon-list-alt"></i> Listado Perfiles</a></li>
							        <li class="btn btn-default btn-block list-group-item pull-left loadArtistas"><a class="botonEnlaces" href="#"><i class="glyphicon glyphicon-picture"></i> Listado Artistas</a></li>
							        <li class="btn btn-default btn-block list-group-item pull-left loadCategorias"><a class="botonEnlaces" href="#"><i class="glyphicon glyphicon-align-left"></i> Listado Categorías</a></li>
							        <li class="btn btn-default btn-block list-group-item pull-left loadImagenes"><a class="botonEnlaces" href="#"><i class="glyphicon glyphicon-camera"></i> Listado Imágenes</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>