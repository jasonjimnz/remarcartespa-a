<body>
	<!-- Cabecera -->
	<header class="row">
		<nav class="navbar navbar-default col-lg-12 col-md-12 col-sm-12">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Reinventarte</a>
		    </div>

		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#"><i class="glyphicon glyphicon-home"></i> Inicio <span class="sr-only">(actual)</span></a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-user"></i> Usuarios <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		            <li><a href="#"><i class="glyphicon glyphicon-th-list"></i> Listado</a></li>
		            <li><a href="#"><i class="glyphicon glyphicon-list-alt"></i> Perfiles</a></li>
		            <li><a href="#"><i class="glyphicon glyphicon-picture"></i> Artistas</a></li>
		          </ul>
		        </li>
		      </ul>
		      <form class="navbar-form navbar-right" role="search">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Búsqueda">
		        </div>
		        <button type="submit" class="btn btn-default">Buscar</button>
		      </form>
		      <ul class="nav navbar-nav navbar-left">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-briefcase"></i> Administración <span class="caret"></span></a>
		          <ul class="dropdown-menu" role="menu">
		          	<li><a href="#"><span>Hola: </span><b><?php echo 'Hola '.$datos['perfil']['nombre']; ?></b></a></li>
		            <li><a href="#"><i class="glyphicon glyphicon-envelope"></i> Mensajes</a></li>
		            <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Mi cuenta</a></li>
		            <li><a href="#"><i class="glyphicon glyphicon-log-out"></i> Cerrar sesión</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</header>
	<!-- Fin cabecera -->