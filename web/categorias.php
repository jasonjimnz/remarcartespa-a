<table class="table table-striped table-hover">
	<thead>
		<tr>
			<td>Nombre</td>
			<td>Descripción</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		include 'config/config.php';
		$categorias = new Categoria;
		$variable = $categorias->all($db);

		$resultados = $variable->fetchAll();
		foreach ($resultados as $row) {
			?>
			<tr>
				<td><?php echo $row['nombre']; ?></td>
				<td><?php echo $row['descripcion']; ?></td>
			</tr>
			<?php

		}
		if ($variable->rowCount() == 0){
			echo '<tr><td>No se han encontrado resultados</td><td><td></tr>';
		}
	?>
	</tbody>
</table>

<h4>Creación de categorías</h4>

<form method="POST" action="javascript:void(0);" class="formularioCategorias">
	<div class="form-group">
		<label for="categoria_nombre">Nombre:</label>
    	<input type="text" class="form-control" id="categoria_nombre" name="nombre" placeholder="Nombre de la categoría">
	</div>

	<div class="form-group">
		<label for="categoria_descripcion">Descripción:</label>
    	<textarea type="text" class="form-control" id="categoria_descripcion" placeholder="Descripción de la categoría"></textarea>
	</div>

	<button class="btn btn-block btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>Guardar</button>
</form>