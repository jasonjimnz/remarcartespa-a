<table class="table table-striped table-hover">
	<thead>
		<tr>
			<td>Nombre</td>
			<td>Apellidos</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		include 'config/config.php';
		$perfiles = new Perfil;
		$variable = $perfiles->all($db);

		$resultados = $variable->fetchAll();
		foreach ($resultados as $row) {
			?>
			<tr>
				<td><?php echo $row['nombre']; ?></td>
				<td><?php echo $row['apellidos']; ?></td>
			</tr>
			<?php
		}
		if ($variable->rowCount() == 0){
			echo '<tr><td>No se han encontrado resultados</td><td><td></tr>';
		}
	?>
	</tbody>
</table>

