<table class="table table-striped table-hover">
	<thead>
		<tr>
			<td>Nombre</td>
			<td>Descripción</td>
			<td>Url</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		include 'config/config.php';
		$imagen = new Imagen;
		$variable = $imagen->all($db);

		$resultados = $variable->fetchAll();
		foreach ($resultados as $row) {
			?>
			<tr>
				<td><?php echo $row['nombre']; ?></td>
				<td><?php echo $row['descripcion']; ?></td>
				<td><?php echo $row['url']; ?></td>
			</tr>
			<?php
		}

		if ($variable->rowCount() == 0){
			echo '<tr><td>No se han encontrado resultados</td><td></td><td></td></tr>';
		}
	?>
	</tbody>
</table>

