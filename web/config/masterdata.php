<?php 

	class Comunidad{
		function create($db, $nombre, $codigo){
			$datos = array(
				':nombre' => $nombre,
				':codigo' => $pass,
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s"),
				':alive' => 1
				);
			$consulta = $db->prepare("INSERT INTO core_comunidad(nombre,codigo,alive,created_at,updated_at) 
				VALUES(:nombre,:codigo,:alive,:created_at,:updated_at)");
			$consulta->execute($datos);
			return $consulta;
		}

		function update($db, $nombre, $codigo, $id){
			$datos = array(
				$nombre,
				$codigo,
				date("Y-m-d H:i:s"),
				$id
			);
			$consulta = $db->prepare("UPDATE core_comunidad SET nombre=? , codigo=? , updated_at =? WHERE id=?");
			$consulta->execute($datos);
			return $consulta;
		}

		function delete($db, $id){

		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_comunidad");
			return $consulta;
		}
	}
	class Provincia{
		function create($db, $nombre, $codigo, $comunidad){
			$datos = array(
				':nombre' => $nombre,
				':codigo' => $codigo,
				':comunidad' => $comunidad,
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s"),
				':alive' => 1
				);
			$consulta = $db->prepare("INSERT INTO core_provincia(nombre,codigo,comunidad_id,created_at,updated_at,alive)
				VALUES(:nombre,:codigo,:comunidad,:created_at,:updated_at,:alive");
			$consulta->execute($datos);

			return $consulta;
		}

		function update($db, $nombre, $codigo, $comunidad, $id){
			$datos = array(
				$nombre,
				$codigo,
				$comunidad,
				date("Y-m-d H:i:s"),
				$id
				);
			$consulta = $db->prepare("UPDATE core_provincia SET nombre=?, codigo=?, comunidad=?, updated_at =? WHERE id=?");
			$consulta->execute($datos);
			return $consulta;
		}

		function delete($db, $id){

		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_provincia");
			return $consulta;
		}
	}
	class Categoria{
		function create($db, $nombre, $descripcion){
			$datos = array(
				':nombre' => $nombre,
				':descripcion' => $descripcion,
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s"),
				':alive' => 1
				);
			$consulta = $db->prepare("INSERT INTO core_categoria(nombre,descripcion,created_at,updated_at,alive)
				VALUES(:nombre,:descripcion,:created_at,:updated_at,:alive");
			$consulta->execute($datos);

			return $consulta;
		}

		function update($db, $nombre, $descripcion, $id){
			$datos = array(
				$nombre,
				$descripcion,
				date("Y-m-d H:i:s"),
				$id
				);
			$consulta = $db->prepare("UPDATE core_categoria SET nombre=?, descripcion=?, updated_at=? WHERE id=?");
			$consulta->execute($datos);

			return $consulta;
		}

		function delete($db, $id){

		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_categoria");
			return $consulta;
		}
	}

	class Imagen{
		function create($db, $nombre, $descripcion, $url, $artista, $categoria){
			$datos = array(
				':nombre' => $nombre,
				':descripcion' => $descripcion,
				':url' => $url,
				':artista' => $artista,
				':categoria' => $categoria,
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s"),
				':alive' => 1
				);
			$consulta = $db->prepare("INSERT INTO core_imagen(nombre,descripcion,url,artista,categoria,created_at,updated_at,alive)
				VALUES(:nombre,:descripcion,:url,:artista,:categoria,:created_at,:updated_at,:alive");
			$consulta->execute($datos);

			return $consulta;
		}

		function update($db, $nombre, $descripcion, $url, $artista, $categoria, $id){
			$datos = array(
				$nombre,
				$descripcion,
				$url,
				$artista,
				date("Y-m-d H:i:s"),
				$id
				);
			$consulta = $db->prepare("UPDATE core_imagen SET nombre =?, descripcion =?, url =?, artista=?, updated_at=? WHERE id=?");
			$consulta->execute($datos);

			return $consulta;
		}

		function delete($db, $id){

		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_imagen");
			return $consulta;
		}
	}

 ?>