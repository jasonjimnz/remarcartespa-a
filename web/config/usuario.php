	<?php 
	class Usuario {
		function nuevoUsuario($db, $email, $pass){
			$datos = array(
				':email' => $email,
				':auth' => sha1($pass),
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s"),
				':admin' => 0,
				':active' => 1,
				':alive' => 0
				);
			$consulta = $db->prepare("INSERT INTO core_usuario(email,auth,admin,alive,activo,created_at,updated_at) 
				VALUES(:email,:auth,:admin,:alive,:active,:created_at,:updated_at)");
			$consulta->execute($datos);
			return $consulta;
		}

		function editarUsuario($db, $email, $pass, $id){
			$datos = array(
				$email,
				sha1($pass),
				$nombre,
				date("Y-m-d H:i:s"),
				$id
			);
			$consulta = $db->prepare("UPDATE core_usuario SET email=? , pass=? , username=?, updated_at =? WHERE id=?");
			$consulta->execute($datos);
			return $consulta;
		}

		function borrarUsuario($db, $id){
			$consulta = $db->prepare("DELETE FROM core_perfil WHERE id=:id");
			$consulta->bindValue(':id', $id, PDO::PARAM_STR);
			$consulta->execute();

			$consulta = $db->prepare("DELETE FROM core_usuario WHERE id=:id");
			$consulta->bindValue(':id', $id, PDO::PARAM_STR);
			$consulta->execute();

			$filas = $consulta->rowCount();
			if ($filas == 0) {
				return false;
			} else {
				return true;
			}
		}

		function inhabilitarUsuario($db, $id){
			$consulta = $db->prepare("UPDATE core_usuario SET active=0 WHERE id=?");
			$consulta->execute(array($id));

			return $consulta;
		}

		function habilitarUsuario($db, $id){
			$consulta = $db->prepare("UPDATE core_usuario SET active=1 WHERE id=?");
			$consulta->execute(array($id));

			return $consulta;
		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_usuario");
			return $consulta;
		}

		function filtrarUsuariosActivos($db, $actividad){
			if($actividad != 0 && $actividad != 1){
				return false;
			}
			$consulta = $db->prepare("SELECT * FROM core_usuario WHERE active =?");
			$consulta->execute(array($actividad));

			return $consulta;
		}

		function loginUsuario($db, $usuario, $pass){
			$consulta = $db->prepare("SELECT * FROM core_usuario WHERE email =? AND auth =?");
			$consulta->execute(array($usuario, sha1($pass)));

			$datos = $consulta->fetch();

			$consulta2 = $db->prepare("SELECT * FROM core_perfil WHERE usuario_id =?");
			$consulta2->execute(array($datos['id']));

			$respuesta = array('usuario' => $datos , 'perfil' => $consulta2->fetch());

			return $respuesta;
		}
	}

	class Perfil{
		function crearPerfil($db,$nombre, $apellidos, $provincia, $localidad, $telefono){
			$datos = array(
				':nombre' => $nombre,
				':apellidos' => $apellidos,
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s"),
				':provincia' => $provincia,
				':localidad' => $localidad,
				':telefono' => $telefono
				);
			$consulta = $db->prepare("INSERT INTO core_perfil(nombre,apellidos,provincia,localidad,telefono,created_at,updated_at) 
				VALUES(:nombre,:apellidos,:provincia,:localidad,:telefono,:created_at,:updated_at)");
			$consulta->execute($datos);
			return $consulta;
		}

		function actualizarPerfil($db, $id, $nombre, $apellidos, $provincia, $localidad, $telefono){
			$datos = array(
				$nombre,
				$apellidos,
				$provincia,
				$localidad,
				$telefono,
				date("Y-m-d H:i:s"),
				$id
				);

			$consulta = $db->prepare("UPDATE core_perfil SET nombre=? , apellidos=? , provincia=?, localidad=?, telefono=?, updated_at =? WHERE id=?");
			$consulta->execute($datos);
			return $consulta;
		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_perfil");
			return $consulta;
		}
	}

	class Artista{
		function create($db, $nick, $usuario){
			$datos = array(
				':nick' => $nick,
				':usuario' => $usuario,
				':alive' => $alive,
				':created_at' => date("Y-m-d H:i:s"),
				':updated_at' => date("Y-m-d H:i:s")
				);
			$consulta = $db->prepare("INSERT INTO core_artista(nick,usuario,alive,created_at,updated_at)
				VALUES(:nick,:usuario,:alive,:created_at,:updated_at");
			$consulta->execute($datos);
			return $consulta;
		}

		function update($db, $nick, $usuario, $id){
			$datos = array(
				$nick,
				$usuario,
				date("Y-m-d H:i:s"),
				$id
				);
			$consulta = $db->prepare("UPDATE core_artista SET nick =? , usuario_id=?, updated_at=?, WHERE id=?");
			$consulta->execute($datos);

			return $consulta;
		}

		function delete($db, $id){

		}

		function all($db){
			$consulta = $db->query("SELECT * FROM core_artista");
			return $consulta;
		}
	}
?>