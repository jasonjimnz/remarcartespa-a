<?php 

	include 'config/config.php';

	$orden = '';


	try{
		$orden = $_GET['orden'];
	} catch(Exception $e){
		//Error sin orden
		die();
	}

	switch ($orden) {
		case 'login':
			header('Content-Type: application/json');
			$json = file_get_contents('php://input');
			$obj = json_decode($json, true);

			$repuesta = $usuario->loginUsuario($db, $obj['login'], $obj['auth']);

			if($repuesta['usuario']['id'] == ''){
				http_response_code(404);
				die();
			}

			$json = "{";
			$json .= '"email":"'.$repuesta['usuario']['email'].'",';
			$json .= '"id_usuario":"'.$repuesta['usuario']['id'].'",';
			$json .= '"nombre":"'.$repuesta['perfil']['nombre'].'",';
			$json .= '"apellidos":"'.$repuesta['perfil']['apellidos'].'"';
			$json .= "}";

			print_r($json);
			break;

		case 'registro':
			header('Content-Type: application/json');
			$json = file_get_contents('php://input');
			$obj = json_decode($json, true);

			$repuesta = $usuario->nuevoUsuario($db, $obj['login'], $obj['auth']);

			if($repuesta->rowCount() == 0){
				http_response_code(400);
				die();
			} else {
				http_response_code(201);
				#print_r('{"status":"Registro OK"}');
			}
			break;
		
		default:
			# code...
			break;
	}

 ?>