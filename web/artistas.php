<table class="table table-striped table-hover">
	<thead>
		<tr>
			<td>Nick</td>
			<td>Usuario id</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		include 'config/config.php';
		$artistas = new Artista;
		$variable = $artistas->all($db);

		$resultados = $variable->fetchAll();
		foreach ($resultados as $row) {
			?>
			<tr>
				<td><?php echo $row['nick']; ?></td>
				<td><?php echo $row['usuario_id']; ?></td>
			</tr>
			<?php
		}

		if ($variable->rowCount() == 0){
			echo '<tr><td>No se han encontrado resultados</td><td><td></tr>';
		}
	?>
	</tbody>
</table>

