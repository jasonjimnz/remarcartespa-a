<table class="table table-striped table-hover">
	<thead>
		<tr>
			<td>Email</td>
			<td>Moderador</td>
		</tr>
	</thead>
	<tbody>
	<?php 
		include 'config/config.php';
		$variable = $usuario->all($db);

		$resultados = $variable->fetchAll();
		foreach ($resultados as $row) {
			?>
				<td><?php echo $row['email']; ?></td>
				<td><?php echo $row['moderador']; ?></td>
			<?php
		}

		if ($variable->rowCount() == 0){
			echo '<tr><td>No se han encontrado resultados</td><td></td></tr>';
		}
	?>
	</tbody>
</table>

