<?php 
	$accion = false;
	include 'config/config.php';

	try{
		if (isset($_POST['session'])){
			$respuesta = $usuario->loginUsuario($db, $_POST['login'], $_POST['pass']);
			$datos = $respuesta;
			if($datos['usuario']['id'] == ''){
				header("Location: index.php");
			}
			$accion = true;
		}
	} catch (Exception $e){
		//Error
	}
 ?>

<?php 
		include 'includes/head.php';
	if ($accion == true){
		include 'includes/header.php';
		include 'includes/menu-izq.php';
		include 'includes/content.php';
		include 'includes/footer.php';
	} else {
 ?>

 	<body style="padding: 15% 0;">
 		<div class="row">
 			<div class="col-lg-6 col-lg-offset-3">
 				<div class="panel panel-default">
 					<div class="panel-heading">
 						<h3 class="panel-title text-center">Login</h3>
 					</div>
 					<div class="panel-body">
 						<form action="index.php" method="POST">
 							<input type="hidden" name="session" value="true">
 							<div class="form-group">
 								<label>Login:</label>
 								<input type="text" placeholder="Login" name="login" class="form-control">
 							</div>

 							<div class="form-group">
 								<label>Contraseña:</label>
 								<input type="text" placeholder="Contraseña" name="pass" class="form-control">
 							</div>

 							<button type="submit" class="btn btn-block btn-primary"><i class="glyphicon glyphicon-log-in"></i> Inicia sesión</button>
 							<a type="button" href="registro.php" class="btn btn-block btn-primary"><i class="glyphicon glyphicon-plus"></i> Registrarse</a>
 						</form>
 					</div>
 				</div>
 			</div>
 		</div>
 	</body>

<?php } ?>